#!/usr/bin/env python3

import os
import bs4
import requests
import pickle
import sys
import math

def penalty(rank):
    if rank < 10:
        return 0
    return math.sqrt(2)/math.sqrt(1 + math.exp(2000/rank))

words = []
urls = [
    "https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/1-10000",
    "https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/10001-20000",
    "https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/20001-30000",
    "https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/30001-40000"
]
for url in urls:
    sys.stdout.write(f"Fetching {url}...")
    soup = bs4.BeautifulSoup(requests.get(url).text, "html.parser")
    sys.stdout.write("done\n")
    for rows in soup.find_all("table"):
        for tr in rows.find_all("tr"):
            if tr.text != "\n":
                words.append([td.text for td in tr.find_all("td")][1])

penalties = {word:penalty(rank+1) for rank,word in enumerate(words)}

with open(os.path.join(os.path.dirname(__file__), "penalties.pickle"), "wb") as w:
    pickle.dump(penalties, w)
