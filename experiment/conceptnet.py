from enum import Enum
from collections.abc import Iterator
import pickle
import os

import numpy as np
import requests


class Relevancy(Enum):
    is_food = 0
    food_related = 1
    cooking = 2
    none = 3

class WordPenalizer:
    penalties:dict = pickle.load(open(os.path.join(os.path.dirname(__file__), "penalties.pickle"), "rb"))
    relevancy_penalties: dict = {
        Relevancy.is_food: 1,
        Relevancy.food_related: 0.2,
        Relevancy.cooking: 0,
        Relevancy.none: 0
    }
    @classmethod
    def penalty(cls, word, relevancy: Relevancy) -> int:
        return cls.penalties.get(word, 1) * cls.relevancy_penalties.get(relevancy, 0)
    

class Vectorizer:
    def __init__(self, lemmas: 'set'):
        self.indices = {}
        self.lemmas = []
        for idx, lemma in enumerate(lemmas):
            self.indices[lemma] = idx
            self.lemmas.append(lemma)

    def vectorize(self, lemmas: Iterator[tuple[str, Relevancy]]) -> np.ndarray:
        res = np.zeros((len(self.indices),))
        for lemma, relevant in lemmas:
            # TODO: penalize common words.
            if relevant != Relevancy.none:
                res[self.indices[lemma]] += WordPenalizer.penalty(lemma, relevant)
        return res


class ConceptNetHandler:
    cache: 'dict[str,Relevancy]' = {}
    lemmas_encountered = set()
    food_identifiers = ['food', 'fruit']

    @classmethod
    def relation_exists(cls, phrase, category, rel="") -> 'Relevancy':
        if rel:
            rel = f"&rel=/r/{rel}"
        url = f"https://api.conceptnet.io/query?node=/c/en/{phrase}&limit=1&other=/c/en/{category}{rel}"
        cls.lemmas_encountered.add(phrase)
        if url not in cls.cache:
            print(url)
            cls.cache[url] = requests.get(
                url).json().get('edges', None)
        return cls.cache[url]

    @classmethod
    def relevant(cls, phrase: str) -> 'Relevancy':
        for identifier in cls.food_identifiers:
            if cls.relation_exists(phrase, identifier, 'IsA'):
                return Relevancy.is_food
        if cls.relation_exists(phrase, 'food'):
            return Relevancy.food_related
        elif cls.relation_exists(phrase, 'cook'):
            return Relevancy.cooking
        else:
            cls.lemmas_encountered.discard(phrase)
            return Relevancy.none
    
    @classmethod
    def get_vectorizer(cls) -> Vectorizer:
        return Vectorizer(cls.lemmas_encountered)


class DishForest:
    def __init__(self, name, model):
        self.model = model
        self.name = name
        self.doc = self.model(self.name)
        self.trees = list(self.doc.noun_chunks)

    def __repr__(self):
        return f"DishForest({self.trees})"

    def food_words(self) -> Iterator[tuple[str, Relevancy]]:
        for tree in self.trees:
            for word in tree:
                result = ConceptNetHandler.relevant(word.lemma_.lower())
                if result != Relevancy.none:
                    yield word.lemma_.lower(), result
