{
  description = "A very basic flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [poetry jupyter];
          shellHook = ''
            if ! poetry env info --path 2>&1 > dev/null; then
              poetry install
            fi
            source `poetry env info --path`/bin/activate
          '';
        };
        formatter = nixpkgs.legacyPackages."${system}".nixfmt;
      });
}
